<?php

$id = $_GET["id"];
require __DIR__ . '/vendor/autoload.php';

?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <title>Películas | Ficha</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/estilos.css">
</head>

<body>
    <div class="alert alert-secondary d-flex">
        <a href="./peliculas.php" class="btn btn-dark">Películas</a>&nbsp;&nbsp;
    </div>
    <div class="container">

        <!-- Código PHP -->
        <?php
        $directores = include('bbdd/pelicula_director.php');
        $actores = include('bbdd/pelicula_actor.php');
        $datosDirectores = include('bbdd/directores.php');
        $datosActores = include('bbdd/actores.php');

        function getValue($nombreKey, $id)
        {
            $peliculas = include('bbdd/peliculas.php');
            $expression = "[?id == `$id`].$nombreKey";
            $value = JmesPath\search($expression, $peliculas);

            return $value;
        }

        function getId($array, $id, $nombreKey)
        {
            $expression = "[?id_pelicula == `$id`].$nombreKey";
            $ids = JmesPath\search($expression, $array);

            return $ids;
        }

        function getValues($array, $ids)
        {
            $values = array();
            foreach ($ids as $id) {
                $expression = "[?id == `$id`].{id: id, nombre: nombre}";
                $element = JmesPath\search($expression, $array);

                array_push($values, $element[0]);
            }

            return $values;
        }

        $titulo = getValue('titulo', $id);
        $anyo = getValue('anyo', $id);
        $duracion = getValue('duracion', $id);

        $idDirectores = getId($directores, $id, 'id_director');
        $idActores = getId($actores, $id, 'id_actor');

        $directoresValues = getValues($datosDirectores, $idDirectores);
        $actoresValues = getValues($datosActores, $idActores);

        ?>

        <b>Título: </b> <?php echo implode(" ", $titulo); ?> <br />
        <b>Año: </b><?php echo implode(" ", $anyo); ?><br />
        <b>Duración: </b><?php echo implode(" ", $duracion); ?><br />

        <b>Director: </b>
        <?php foreach ($directoresValues as $value) : ?>
            <a href='./directores_ficha.php?id=<?php echo $value["id"]; ?>'>
                <li><?php echo $value["nombre"]; ?></li>
            </a>
        <?php endforeach; ?>

        <b>Actores: </b>
        <?php foreach ($actoresValues as $value) : ?>
            <a href='./actores_ficha.php?id=<?php echo $value["id"]; ?>'>
                <li><?php echo $value["nombre"]; ?></li>
            </a>
        <?php endforeach; ?>

        <!-- Fin código PHP -->

    </div>
</body>

</html>