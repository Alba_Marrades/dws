<?php 

$id = $_GET["id"];
require __DIR__ . '/vendor/autoload.php';

?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <title>Directores | Ficha</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/estilos.css">
</head>

<body>
    <div class="alert alert-secondary d-flex">
        <a href="./peliculas.php" class="btn btn-dark">Películas</a>&nbsp;&nbsp;
    </div>
    <div class="container">

        <!-- Código PHP -->
        <?php

        function getValues($id)
        {
            $actores = include('bbdd/actores.php');

            $expression = "[?id == `$id`].{nombre: nombre, anyo: anyo, pais: pais}";
            $element = JmesPath\search($expression, $actores);

            $values = $element[0];

            return $values;
        }

        $actor = getValues($id);

        ?>
        
        <b>Nombre: </b><?php echo $actor["nombre"]; ?><br />
        <b>Año: </b><?php echo $actor["anyo"]; ?><br />
        <b>País: </b><?php echo $actor["pais"]; ?><br />
        <!-- Fin código PHP -->

    </div>
</body>

</html>