<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mostrar fichero</title>
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<?php
    $palabras = file("palabras.txt");
    sort($palabras);
    $archivo = fopen("palabras.txt", "w");
    foreach ($palabras as $palabra)
        fwrite($archivo, $palabra);
    fclose($archivo);
    $archivo = fopen("palabras.txt", "r");
    While(!feof($archivo)) {
        echo fgets($archivo) . "<br/>";
    }
    fclose($archivo);
?>    
</body>
</html>