<?php

class Calculadora
{
    private $numero1 = 8;

    public function sumar($numero2) {
        return $this->numero1 + $numero2;
    }

    public function restar($numero2) {
        return $this->numero1 - $numero2;
    }

    public function multiplicar($numero2) {
        return $this->numero1 * $numero2;
    }

    public function dividir($numero2) {
        return $this->numero1 / $numero2;
    }
}

$calculadora = new Calculadora();

echo $calculadora->sumar(2) . "<br>";
echo $calculadora->restar(2) . "<br>";
echo $calculadora->multiplicar(2) . "<br>";
echo $calculadora->dividir(2) . "<br>";
