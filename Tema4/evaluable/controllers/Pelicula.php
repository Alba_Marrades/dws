<?php

namespace controllers;

use core\Controller;
use models\Pelicula as modelPelicula;
use models\Director as modelDirector;
use models\Actor as modelActor;

class Pelicula extends Controller
{
    function getAll()
    {
        $peliculas = modelPelicula::getAll();
        echo $this->templates->render('peliculas_listado', ['peliculas' => $peliculas]);
    }

    function getById($vars)
    {
        $pelicula = modelPelicula::getById($vars['id']);
        $directores = modelDirector::getByPelicula($vars['id']);
        $actores = modelActor::getByPelicula($vars['id']);
        echo $this->templates->render('peliculas_ficha', ['pelicula' => $pelicula, 'directores' => $directores, 'actores' => $actores]);
    }
}
