<?php

namespace models;

use core\Model;

class Persona extends Model
{
    protected $key;
    protected $tablaRelacion;

    protected function getByPelicula($id)
    {
        $personasPelicula = include('./bbdd/' . $this->tablaRelacion);
        $expression = "[?id_pelicula == `$id`].$this->key";
        $ids = \JmesPath\search($expression, $personasPelicula);

        $datosPersona = include('./bbdd/' . $this->table);
        $values = array();
        foreach ($ids as $id) {
            $expression = "[?id == `$id`].{id: id, nombre: nombre}";
            $element = \JmesPath\search($expression, $datosPersona);

            array_push($values, $element[0]);
        }

        return $values;
    }
}
