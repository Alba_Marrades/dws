<?php

namespace models;

class Director extends Persona
{
    protected $table = 'directores.php';
    protected $tablaRelacion = 'pelicula_director.php';
    protected $key = 'id_director';
}