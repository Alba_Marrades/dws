<?php

namespace models;

class Actor extends Persona
{
    protected $table = 'actores.php';
    protected $tablaRelacion = 'pelicula_actor.php';
    protected $key = 'id_actor';
}