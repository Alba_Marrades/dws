<?php

abstract class Trabajador 
{
    protected $nombre;
    protected $sueldo;

    public function __construct($nombre)
    {
        $this->nombre = $nombre;
    }

    abstract public function calcularSueldo(); 
    
    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }
}

class Empleado extends Trabajador 
{
    public function __construct($nombre)
    {   
        parent::__construct($nombre);
        $this->sueldo = 9.5;        
    }

    public function calcularSueldo() {
        return ($this->sueldo * 8) * 30;
    }
}

class Gerente extends Trabajador
{
    protected $beneficio;

    public function __construct($nombre, $beneficio)
    {   
        parent::__construct($nombre);
        $this->beneficio = $beneficio;
        $this->sueldo = 2500;
    }

    public function calcularSueldo()
    {
        return $this->sueldo + $this->beneficio;
    }
}

$empleado = new Empleado("Roberto");
$gerente = new Gerente("Luisa", 400);

echo "El sueldo de " . $empleado->getNombre() . " es de " . $empleado->calcularSueldo() . "€.";
echo "<br>";
echo "El sueldo de " . $gerente->getNombre() . " es de " . $gerente->calcularSueldo() . "€.";

