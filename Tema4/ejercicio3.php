<?php

class Empleado {
    private $nombre;
    private $sueldo;

    public function __construct($nombre, $sueldo)
    {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    public function impuestos() {
        if ($this->sueldo > 1200) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of sueldo
     */ 
    public function getSueldo()
    {
        return $this->sueldo;
    }

}

function muestraImpuestos($empleado) {
    if ($empleado->impuestos()) {
        echo $empleado->getNombre() . " tiene que pagar impuestos.<br>";
    } else {
        echo $empleado->getNombre() . " no tiene que pagar impuestos.<br>";
    }
}

$empleado1 = new Empleado("Pepe", 1000);
$empleado2 = new Empleado("Carmen", 2500);

echo $empleado1->getNombre() . " tiene un sueldo de " . $empleado1->getSueldo() . " €.<br>";
muestraImpuestos($empleado1);
echo $empleado2->getNombre() . " tiene un sueldo de " . $empleado2->getSueldo() . " €.<br>";
muestraImpuestos($empleado2);