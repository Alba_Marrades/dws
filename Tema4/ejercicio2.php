<?php

class Empleado {
    private $nombre;
    private $sueldo;

    public function __construct($nombre, $sueldo)
    {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of sueldo
     */ 
    public function getSueldo()
    {
        return $this->sueldo;
    }

}

$empleado1 = new Empleado("Pepe", 1000);
$empleado2 = new Empleado("Carmen", 2500);

echo $empleado1->getNombre() . " tiene un sueldo de " . $empleado1->getSueldo() . " €.<br>";
echo $empleado2->getNombre() . " tiene un sueldo de " . $empleado2->getSueldo() . " €.";