<?php

class Empleado {
    private $nombre;
    private $sueldo;

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of sueldo
     */ 
    public function getSueldo()
    {
        return $this->sueldo;
    }

    /**
     * Set the value of sueldo
     *
     * @return  self
     */ 
    public function setSueldo($sueldo)
    {
        $this->sueldo = $sueldo;

        return $this;
    }
}

$empleado1 = new Empleado();
$empleado2 = new Empleado();

$empleado1->setNombre("Pepe");
$empleado1->setSueldo(1000);
$empleado2->setNombre("Carmen");
$empleado2->setSueldo(2500);

echo $empleado1->getNombre() . " tiene un sueldo de " . $empleado1->getSueldo() . " €.<br>";
echo $empleado2->getNombre() . " tiene un sueldo de " . $empleado2->getSueldo() . " €.";