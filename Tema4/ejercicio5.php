<?php

class Calculadora
{
    public function sumar($numero1, $numero2) {
        return $numero1 + $numero2;
    }

    public function restar($numero1, $numero2) {
        return $numero1 - $numero2;
    }

    public function multiplicar($numero1, $numero2) {
        return $numero1 * $numero2;
    }

    public function dividir($numero1, $numero2) {
        return $numero1 / $numero2;
    }
}

$calculadora = new Calculadora();

echo $calculadora->sumar(5,2) . "<br>";
echo $calculadora->restar(5,2) . "<br>";
echo $calculadora->multiplicar(5,2) . "<br>";
echo $calculadora->dividir(5,2) . "<br>";

