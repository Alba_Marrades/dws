<?php

class Mamifero
{
    private $especie;
    private $sonido;
    protected $familia;

    public function __construct($especie, $sonido)
    {
        $this->especie = $especie;
        $this->sonido = $sonido;
    }

    public function sonido()
    {
        echo "Sonido de " . $this->especie . " , de la familia " . $this->familia . ": " . $this->sonido . ".";
    }

}

class Perro extends Mamifero
{
    public function __construct($especie, $sonido)
    {
        parent::__construct($especie, $sonido);
        $this->familia = "cánido";
    }
}

class Gato extends Mamifero
{
    public function __construct($especie, $sonido)
    {
        parent::__construct($especie, $sonido);
        $this->familia = "felino";
    }

}

$perro = new Perro("galgo", "woof!");
$gato = new Gato("siamés", "mew");

$perro->sonido();
echo "<br>";
$gato->sonido();