<?php

class Calculadora
{
    static private $numero1 = 8;

    private function sumar($numero2)
    {
        return self::$numero1 + $numero2;
    }

    private function restar($numero2)
    {
        return self::$numero1 - $numero2;
    }

    private function multiplicar($numero2)
    {
        return self::$numero1 * $numero2;
    }

    private function dividir($numero2)
    {
        return self::$numero1 / $numero2;
    }

    public static function __callStatic($name, $arguments)
    {
        return (new static)->$name(...$arguments);
    }
}

echo Calculadora::sumar(2) . "<br>";
echo Calculadora::restar(2) . "<br>";
echo Calculadora::multiplicar(2) . "<br>";
echo Calculadora::dividir(2) . "<br>";
