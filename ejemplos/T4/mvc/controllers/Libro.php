<?php

namespace controllers;

use core\Controller;
use models\Libro as modelLibro;

class Libro extends Controller
{
    function getById($vars)
    {
        $libro = modelLibro::getById($vars['id']);
        echo $this->templates->render('libros_ficha', ['libro' => $libro]);
    }

    function getAll()
    {
        $libros = modelLibro::getAll();
        echo $this->templates->render('libros_listado', ['libros' =>
        $libros]);
    }
}
